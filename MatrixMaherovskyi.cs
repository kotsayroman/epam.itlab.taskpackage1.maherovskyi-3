using System;
using System.Text;

namespace Epam.ITLab.TestPackage1.Matrix.Maherovskyi
{
    /// <summary>
    /// Realises rectangle matrix and the main operations on it.
    /// </summary>
    class Matrix
    {
        /// <summary>
        /// Matrix of an any size
        /// </summary>
        double[,] _matrix;

        /// <summary>
        /// Class constructor.
        /// </summary>
        public Matrix(double[,] matrix)
        {
            if(matrix.Length == 0)
            {
                throw new ArgumentException("Fill the matrix in!");
            }
            _matrix = matrix;
        }

        /// <summary>
        /// Get the matrix height.
        /// </summary>
        public int Height
        {
            get
            {
                return _matrix.GetLength(0);
            }
        }

        /// <summary>
        /// Get the matrix width.
        /// </summary> 
        public int Width
        {
            get
            {
                return _matrix.Length / Height;
            }
        }

        double this[int x, int y]
        {
            get
            {
                return _matrix[x, y];
            }
            set
            {
                _matrix[x, y] = value;
            }
        }

        /// <summary>
        /// Prints matrix in the proper form.
        /// </summary>
        public override string ToString()
        {
            StringBuilder sb = new StringBuilder(128);
            for (var i = 0; i < Height; i++)
            {
                for (var j = 0; j < Width; j++)
                {
                    if (j == 0)
                    {
                        sb.AppendFormat("|\t{0}\t", _matrix[i, j].ToString("0.00"));
                    }
                    else if (j == Width - 1)
                    {
                        sb.AppendFormat("{0}\t|", _matrix[i, j].ToString("0.00"));
                    }
                    else
                    {
                        sb.AppendFormat("{0}\t", _matrix[i, j].ToString("0.00"));
                    }
                }
                sb.Append("\n");
            }
            return sb.ToString();
        }

        /// <summary>
        /// Creates a submatrix from bigger matrix.
        /// </summary>
        public Matrix Submatrix(int ax, int ay, int bx, int by)
        {
            if (ax < 0 || ay < 0 || bx > Width - 1 || by > Height - 1)
            {
                throw new IndexOutOfRangeException("Matrix does not contain elements with such an index");
            }
            Matrix m = new Matrix(new double[bx - ax + 1, by - ay + 1]);
            for (var i = 0; i < m.Height; i++)
            {
                for (var j = 0; j < m.Width; j++)
                {
                    m[i, j] = this[i + ax, j + ay];
                } 
            }
            //Console.WriteLine(m.ToString());
            return m;
        }

        /// <summary>
        /// Transposes the matrix. Columns become rows.
        /// </summary>
        public void Transpose()
        {
            double[,] transposed = new double[Width, Height];
            for (var i = 0; i < Width; i++)
            {
                for (var j = 0; j < Height; j++)
                {
                    transposed[i, j] = _matrix[j, i];
                }
            }
            _matrix = transposed;
        }

        /// <summary>
        /// Returns the hash code of the matrix.
        /// </summary>
        public override int GetHashCode()
        {
            return _matrix.GetHashCode();
        }

        /// <summary>
        /// Compares elements by their hash code.
        /// </summary>
        public override bool Equals(object obj)
        {
            if (obj == null || !(obj is Matrix))
            {
                return false;
            }

            return this.GetHashCode() == obj.GetHashCode();
        }

        /// <summary>
        /// Overloaded Add() method.
        /// </summary>
        public static Matrix operator +(Matrix a, Matrix b)
        {
            return Add(a, b);
        }

        /// <summary>
        /// Overloaded Sub() method.
        /// </summary>
        public static Matrix operator -(Matrix a, Matrix b)
        {
            return Sub(a, b);
        }

        /// <summary>
        /// 2 overloads of MulNumber() method. matrix * num & num * matrix.
        /// </summary>
        public static Matrix operator *(Matrix a, int multiplier)
        {
            return MulNumber(a, multiplier);
        }

        public static Matrix operator *(int multiplier, Matrix a)
        {
            return MulNumber(a, multiplier);
        }

        /// <summary>
        /// Overload of MulMatrix() method.
        /// </summary>
        public static Matrix operator *(Matrix a, Matrix b)
        {
            return MulMatrix(a, b);
        }

        /// <summary>
        /// Overloads division by number method.
        /// </summary>
        public static Matrix operator /(Matrix a, int divider)
        {
            return DivNumber(a, divider);
        }

        /// <summary>
        /// 2 overloads of Equals(m1, m2) method.
        /// </summary>
        public static bool operator ==(Matrix a, Matrix b)
        {
            return Equals(a, b);
        }

        public static bool operator !=(Matrix a, Matrix b)
        {
            return !Equals(a, b);
        }

        /// <summary>
        /// Adds 2 matrixes if the have the same width or height.
        /// </summary>
        static Matrix Add(Matrix a, Matrix b)
        {
            if(a.Width != b.Width || a.Height != b.Height)
            {
                throw new Exception("Cannot add matrix with different width of height");
            }

            for (var i = 0; i < a.Height; i++)
            {
                for (var j = 0; j < a.Width; j++)
                {
                    a[i, j] += b[i, j];
                }
            }

            return a;
        }

        /// <summary>
        /// Substracts 2 matrixes if they have the same width or height.
        /// </summary>
        static Matrix Sub(Matrix a, Matrix b)
        {
            if (a.Width != b.Width || a.Height != b.Height)
            {
                throw new Exception("Cannot substract matrix with different width of height");
            }

            for (var i = 0; i < a.Height; i++)
            {
                for (var j = 0; j < a.Width; j++)
                {
                    a[i, j] -= b[i, j];
                }
            }

            return a;
        }

        /// <summary>
        /// Multiplies matrix elements by number.
        /// </summary>
        static Matrix MulNumber(Matrix a, int multiplier)
        {
            for (var i = 0; i < a.Height; i++)
            {
                for (var j = 0; j < a.Width; j++)
                {
                    a[i, j] *= multiplier;
                }
            }
            return a;
        }

        /// <summary>
        /// Divides each element of matrix by a number.
        /// </summary>
        static Matrix DivNumber(Matrix a, int division)
        {
            for (var i = 0; i < a.Height; i++)
            {
                for (var j = 0; j < a.Width; j++)
                {
                    a[i, j] /= division;
                }
            }
            return a;
        }

        /// <summary>
        /// Multiplies 2 matrixes if width of 1st matrix == height of second.
        /// </summary>
        static Matrix MulMatrix(Matrix a, Matrix b)
        {
            if (a.Width != b.Height)
            {
                throw new Exception("These matrixes cannot be multiplied");
            }

            double[,] result = new double[a.Height, b.Width];
            for (int i = 0; i < a.Height; i++)
            {
                for (int j = 0; j < b.Width; j++)
                {
                    for (int k = 0; k < b.Height; k++)
                    {
                        result[i, j] += a[i, k] * b[k, j];
                    }
                }
            }
            return new Matrix(result);
        }

        /// <summary>
        /// Checks matrix identity element by element.
        /// </summary>
        /// <returns> True is all elements equals. </returns>
        static bool Equals(Matrix a, Matrix b)
        {
            bool isEquals = true;

            if (a.Height != b.Height || a.Width != b.Width)
            {
                return false;
            }

            for (var i = 0; i < a.Height; i++)
            {
                for (var j = 0; j < a.Width; j++)
                {
                    if (a[i, j] != b[i, j])
                    {
                        isEquals = false;
                        break;
                    }
                }
                if (isEquals == false)
                {
                    break;
                }
            }
            return isEquals;
        }
    }


    class Program
    {
        static void Main(string[] args)
        {
            Matrix m = new Matrix(new double[2, 5] { { 1, 2, 3, 4, 5 },
                                                     { 6, 7, 8, 9, 0 } });
            Console.WriteLine("Matrix height: {0}", m.Height);
            Console.WriteLine("Matrix width: {0}", m.Width);
            Console.WriteLine(m);
            m = 3 * m;
            Console.WriteLine(m);
            m.Transpose();
            Console.WriteLine(m);
            Matrix m1 = new Matrix(new double[3, 2] { { 1, 2 }, { 3, 4 }, { 5, 6 } });
            Matrix m2 = new Matrix(new double[2, 2] { { 1, 2 }, { 3, 4 } });
            Matrix m3 = new Matrix(new double[2, 2] { { 1, 2 }, { 3, 4 } });
            Console.WriteLine(m1 == m2);
            Console.WriteLine(m2 == m3);
            m3 += m2;
            Console.WriteLine(m3);
            Matrix m4 = new Matrix(new double[5, 5] { {1, 2, 3, 4, 5 }, { 1, 2, 3, 4, 5 }, { 1, 2, 3, 4, 5 }, { 1, 2, 3, 4, 5 }, { 1, 2, 3, 4, 5 } });
            Console.WriteLine(m4.ToString());
            Console.WriteLine((m4.Submatrix(1, 1, 3, 3)));
        }
    }
}