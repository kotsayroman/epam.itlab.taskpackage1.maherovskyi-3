using System;
using System.Text;

namespace Epam.TaskPackage1.Vector.Maherovskyi
{
	/// <summary>
	/// Class realises the main operations for 3-dimensional vectors.
	/// </summary>
	class Vector
	{		
		/// <summary>
		/// Class constructor.
		/// <param name="x"> X coordinate of Vector. </param>
		/// <param name="y"> Y coordinate of Vector. </param>
		/// <param name="z"> Z coordinate of Vector. </param>		
		/// </summary>
		public Vector(int x, int y, int z)
		{
			X = x;
			Y = y;
			Z = z;
		}
		
		/// <summary>
		/// Vector coordinates.
		/// </summary>
		int X { get; set; }
		int Y { get; set; }
		int Z { get; set; }
		
		/// <summary>
		/// Returns the length of Vector.
		/// </summary>
		public double Length
		{
			get
			{
				return Math.Sqrt(Math.Pow(X, 2) + Math.Pow(Y, 2) + Math.Pow(Z, 2));	
			}
		}
		
		/// <summary>
		/// Overloaded Add method.
		/// </summary>
		public static Vector operator +(Vector a, Vector b)
		{
			return Add(a, b);
		}
		
		/// <summary>
		/// Overloaded Sub method.
		/// </summary>
		public static Vector operator -(Vector a, Vector b)
		{
			return Sub(a, b);
		}
		
		/// <summary>
		/// Overloaded Greater method.
		/// </summary>
		public static bool operator >(Vector a, Vector b)
		{
			return Greater(a, b);
		}
		
		/// <summary>
		/// Returns opposite value to Greater method.
		/// </summary
		public static bool operator <(Vector a, Vector b)
		{
			return !Greater(a, b);
		}
		
		/// <summary>
		/// Overloaded Equals method.
		/// </summary>
		public static bool operator ==(Vector a, Vector b)
		{
			return a.Equals(b);
		}
		
		/// <summary>
		/// Returns opposite value to Equals method
		/// </summary>
		public static bool operator !=(Vector a, Vector b)
		{
			return !a.Equals(b);
		}
		
		/// <summary>
		/// Returns angle between vectors in radiands.
		/// </summaty>
		public static double Angle(Vector a, Vector b)
		{
			double scalar = MulScalar(a, b);
			double cos = scalar / (a.Length * b.Length);
			return cos * Math.PI / 180;
		}
		
		/// <summary>
		/// Adds 2 Vectors.
		/// </summary>
		static Vector Add(Vector a, Vector b)
		{
			return new Vector(a.X + b.X, a.Y + b.Y, a.Z + b.Z);
		}
		
		/// <summary>
		/// Substracts 2 Vectors.
		/// </summary>
		static Vector Sub(Vector a, Vector b)
		{
			return new Vector(a.X - b.X, a.Y - b.Y, a.Z - b.Z);
		}
		
		/// <summary>
		/// Checks whitch vector is greater by length.
		/// </summary>
		static bool Greater(Vector a, Vector b)
		{
			if (a.Length > b.Length)
			{
				return true;
			}
			else
			{
				return false;
			}
		}
		
		/// <summary>
		/// Returns true if vectors length is similar.
		/// </summary>		
		public override bool Equals(Object obj)
		{
			if (obj == null || !(obj is Vector))
			{
				return false;
			}
			return this.Length == ((Vector)obj).Length;
		}

		/// <summary>
		/// Returns the Vector Hash Code.
		/// </summary>
		public override int GetHashCode()
		{
			return new { X, Y, Z }.GetHashCode();
		}
		
		/// <summary>
		/// Returns the 2 vectors scalar multiplier.
		/// </summary>
		public static double MulScalar(Vector a, Vector b)
		{
			// a · b = ax · bx + ay · by + az · bz
			return a.X * b.X + a.Y * b.Y + a.Z * b.Z;
		}
				
		/// <summary>
		/// Returns the 2 vectors vector multiplier.
		/// </summary>
		public static Vector MulVector(Vector a, Vector b)
		{
			// a × b = {ay*bz - az*by; az*bx - ax*bz; ax*by - ay*bx}
			return new Vector(a.Y * b.Z - a.Z * b.Y, a.Z * b.X - a.X * b.Z, a.X * b.Y - a.Y - a.Y * b.X);
		}
		
		/// <summary>
		/// Returns the triple product value.
		/// </summary>
		public static double TripleProduct(Vector a, Vector b, Vector c)
		{
			return (a.X * b.Y * c.Z) + (a.Y * b.Z * c.X) + (a.Z * b.X * c.Y) - 
					(a.Z * b.Y * c.X) - (a.Y * b.X * c.Z) - (a.X * b.Z * c.Y);
			
		}
				
		/// <summary>
		/// Returns the 3-dimensional vector in the (x, y, z) form.
		/// </summary>
		public override string ToString()
		{
			StringBuilder sb = new StringBuilder();
			sb.AppendFormat("({0}, {1}, {2})", X, Y, Z);
			return sb.ToString();
		}
	}
	
	class Program
	{
		static void Main(string[] args)
		{
			Vector a = new Vector(1, 2, 3);
			Vector b = new Vector(2, 2, 3);
			Vector c = a - b;
			Console.WriteLine(c);
			Console.WriteLine(a == b);
			Console.WriteLine(a != b);
			Console.WriteLine("Length of a = {0}, Length of b = {1}", a.Length.ToString("0.00"), b.Length.ToString("0.00"));
			Console.WriteLine(a > b);
			Console.WriteLine("Angle between a and b = {0} radians", Vector.Angle(a, b));
			Console.WriteLine("a hashcode = {0}", a.GetHashCode());
			double d = Vector.MulScalar(a, b);
			Console.WriteLine("Scalar multiplier of vectors a & b = {0}", d);
			d = Vector.TripleProduct(a, b, c);
			Console.WriteLine("Triple product of vectors a, b, c = {0}", d);
			c = Vector.MulVector(a, b);
			Console.WriteLine("Vector multiplier of vectors a & b = {0}", c);
		}
	}
}